const paragraphs = document.querySelectorAll("p");
paragraphs.forEach (elem => elem.style.backgroundColor = "#ff0000");
console.log(paragraphs);

let elemId = document.getElementById("optionsList");
console.log(elemId);
let parentElement = elemId.parentElement;
console.log(parentElement);
let childElements = elemId.childNodes;
console.log(childElements);
for (elem of childElements) {
  console.log("Nodename:" + elem.nodeName, "Nodetype:" + elem.nodeType);
}

let newText = document.getElementById("testParagraph");
newText.textContent = "This is a paragraph";
console.log(newText);
// Спосіб 1
// let mainHeader = document.querySelectorAll(".main-header>*");
// console.log(mainHeader.children);
// mainHeader.forEach((el) => {
//   console.log(el.innerHTML);
//   el.classList.add("nav-item");
// }); 

// спосіб 2
let mainHeader = document.querySelector(".main-header");
console.log(mainHeader.children);
for (const el of mainHeader.children) {
  console.log(el.innerHTML);
  el.classList.add("nav-item");
}

let sectionTitle = document.querySelectorAll(".section-title");
for (let el of sectionTitle) {
  el.classList.remove("section-title");
}
console.log(sectionTitle);
